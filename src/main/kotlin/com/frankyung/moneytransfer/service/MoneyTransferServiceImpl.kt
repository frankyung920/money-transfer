package com.frankyung.moneytransfer.service

import com.frankyung.moneytransfer.models.Account
import com.frankyung.moneytransfer.models.TransactionRecord
import io.vertx.core.json.JsonArray
import io.vertx.core.logging.LoggerFactory
import java.math.BigDecimal
import java.util.*

class MoneyTransferServiceImpl: MoneyTransferService{
    companion object {
        internal val logger = LoggerFactory.getLogger(MoneyTransferServiceImpl::class.java)!!
    }

    constructor()

    override fun getAccountById(id: String, accounts: MutableList<Account>): Account {
        accounts.forEach {account ->
            if (account.id == id){
                return account
            }
        }
        return Account("", "", "", BigDecimal(0), BigDecimal(0), Date(), Date())
    }

    override fun getAccountBalanceById(id: String, accounts: MutableList<Account>): MutableMap<String, BigDecimal>{
        var map = mutableMapOf<String, BigDecimal>()

        accounts.forEach {account ->
            if (account.id == id){
                map.put("balance", account.balance)
                map.put("availableBalance", account.availableBalance)
            }
        }

        return map
    }

    override fun deposit(id: String, depositAmount: BigDecimal, accounts: MutableList<Account>){
        for(account in accounts){
            if(account.id == id){
                account.availableBalance = account.availableBalance + depositAmount
                account.balance = account.balance + depositAmount
                break
            }
        }
    }

    override fun deductAvailableBalance(id: String, withdrawalAmount: BigDecimal, accounts: MutableList<Account>){
        for(account in accounts){
            if(account.id == id){
                account.availableBalance = account.availableBalance - withdrawalAmount
                break
            }
        }
    }

    override fun deductBalance(id: String, withdrawalAmount: BigDecimal, accounts: MutableList<Account>){
        for(account in accounts){
            if(account.id == id){
                account.balance = account.balance - withdrawalAmount
                break
            }
        }
    }

    override fun createTransRecord(accountNo: String, amount: BigDecimal, currency: String, type: String,  status: String, fromAccountNo: String, targetAccountNo: String, transRecords: MutableList<TransactionRecord>): String{
        var recordId = transRecords.size + 1
        var transactionRecord = TransactionRecord(recordId.toString(), accountNo, currency, amount, type, status, fromAccountNo, targetAccountNo, Date(), Date())
        transRecords.add(transactionRecord)

        return recordId.toString()
    }

    override fun updateRecordStatus(recordId: String, status: String, transRecords: MutableList<TransactionRecord>){
        for (record in transRecords){
            if(record.id == recordId){
                record.status = status
                transRecords.set(transRecords.indexOf(record), record)
            }
        }
    }

    override fun getTransRecordByAccountId(accountId: String, transRecords: MutableList<TransactionRecord>): JsonArray {
        var arr = JsonArray()
        for (record in transRecords){
            if(record.accountNo == accountId){
                arr.add(record.toJson())
            }
        }
        return arr
    }

}

