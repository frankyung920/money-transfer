package com.frankyung.moneytransfer.service

import com.frankyung.moneytransfer.models.Account
import com.frankyung.moneytransfer.models.TransactionRecord
import io.vertx.core.json.JsonArray
import java.math.BigDecimal


interface MoneyTransferService {

    fun getAccountById(id: String, accounts: MutableList<Account>): Account

    fun getAccountBalanceById(id: String, accounts: MutableList<Account>): MutableMap<String, BigDecimal>

    fun deposit(id: String, depositAmount: BigDecimal, accounts: MutableList<Account>)

    fun deductAvailableBalance(id: String, withdrawalAmount: BigDecimal, accounts: MutableList<Account>)

    fun deductBalance(id: String, withdrawalAmount: BigDecimal, accounts: MutableList<Account>)

    fun createTransRecord(accountNo: String, amount: BigDecimal, currency: String, type: String, status: String, fromAccountNo: String, targetAccountNo: String, transRecords: MutableList<TransactionRecord>): String

    fun updateRecordStatus(recordId: String, status: String, transRecords: MutableList<TransactionRecord>)

    fun getTransRecordByAccountId(accountId: String, transRecords: MutableList<TransactionRecord>): JsonArray
}

