package com.frankyung.moneytransfer.models


class FieldName{
    companion object {
        const val idFieldName = "id"
        const val accountNameFieldName = "accountName"
        const val accountNoFieldName = "accountNo"
        const val amountFieldName = "amount"
        const val typeFieldName = "type"
        const val statusFieldName = "status"
        const val currencyFieldName = "currency"
        const val availableBalanceFieldName = "availableBalance"
        const val balanceFieldName = "balance"
        const val createdAtFieldName  = "createdAt"
        const val updatedAtFieldName  = "updatedAt"
        const val targetAccountNoFieldName = "targetAccountNo"
        const val fromAccountNoFieldName = "fromAccountNo"
    }
}