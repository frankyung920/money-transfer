package com.frankyung.moneytransfer.models

import com.frankyung.moneytransfer.common.toCustomBd
import io.vertx.core.json.JsonObject
import java.math.BigDecimal
import java.util.*

data class TransactionRecord (
        var id: String,
        var accountNo: String,
        var currency: String,
        var amount: BigDecimal,
        var type: String,
        var status: String,
        var fromAccountNo: String? = "",
        var targetAccountNo: String? = "",
        override var createdAt: Date?,
        override var updatedAt: Date?

) : Dated {

    constructor(jsonObject: JsonObject) : this(
            id = jsonObject.getString(FieldName.idFieldName),
            accountNo = jsonObject.getString(FieldName.accountNoFieldName),
            currency = jsonObject.getString(FieldName.currencyFieldName),
            amount = jsonObject.getValue(FieldName.amountFieldName).toString().toCustomBd(),
            type = jsonObject.getValue(FieldName.typeFieldName).toString(),
            status = jsonObject.getValue(FieldName.statusFieldName).toString(),
            fromAccountNo = jsonObject.getValue(FieldName.fromAccountNoFieldName).toString(),
            targetAccountNo = jsonObject.getValue(FieldName.targetAccountNoFieldName).toString(),
            createdAt = Date(),
            updatedAt = Date()
    )

    fun toJson(): JsonObject {
        val json = JsonObject()

        json.put(FieldName.idFieldName, id)
        json.put(FieldName.accountNoFieldName, accountNo)
        json.put(FieldName.currencyFieldName, currency)
        json.put(FieldName.amountFieldName, amount.toString())
        json.put(FieldName.typeFieldName, type)
        json.put(FieldName.statusFieldName, status)
        json.put(FieldName.fromAccountNoFieldName, fromAccountNo)
        json.put(FieldName.targetAccountNoFieldName, targetAccountNo)
        createdAt?.let{ json.put(FieldName.createdAtFieldName, createdAt.toString()) }
        updatedAt?.let{ json.put(FieldName.updatedAtFieldName, updatedAt.toString()) }

        return json
    }
}
