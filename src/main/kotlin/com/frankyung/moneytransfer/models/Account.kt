package com.frankyung.moneytransfer.models

import com.frankyung.moneytransfer.common.toCustomBd
import io.vertx.core.json.JsonObject
import java.math.BigDecimal

import java.util.*

data class Account (
        var id: String,
        var accountName: String,
        var currency: String,
        var balance: BigDecimal,
        var availableBalance: BigDecimal,
        override var createdAt: Date?,
        override var updatedAt: Date?

) : Dated {

    constructor(jsonObject: JsonObject) : this(
        id = jsonObject.getString(FieldName.idFieldName),
        accountName = jsonObject.getString(FieldName.accountNameFieldName),
        currency = jsonObject.getString(FieldName.currencyFieldName),
        balance = jsonObject.getValue(FieldName.balanceFieldName).toString().toCustomBd(),
        availableBalance = jsonObject.getValue(FieldName.availableBalanceFieldName).toString().toCustomBd(),
        createdAt = Date(),
        updatedAt = Date()
    )

    fun toJson(): JsonObject {
        val json = JsonObject()

        json.put(FieldName.idFieldName, id)
        json.put(FieldName.accountNameFieldName, accountName)
        json.put(FieldName.currencyFieldName, currency)
        json.put(FieldName.balanceFieldName, balance.toString())
        json.put(FieldName.availableBalanceFieldName, availableBalance.toString())
        createdAt?.let{ json.put(FieldName.createdAtFieldName, createdAt.toString()) }
        updatedAt?.let{ json.put(FieldName.updatedAtFieldName, updatedAt.toString()) }

        return json
    }
}
