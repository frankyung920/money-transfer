package com.frankyung.moneytransfer.models

import io.vertx.core.json.JsonObject
import java.util.*

interface Dated {

  var createdAt: Date?
  var updatedAt: Date?
}

fun JsonObject.putDated(dated: Dated): JsonObject {
  dated.createdAt?.let { this.put(FieldName.createdAtFieldName, it) }
  dated.updatedAt?.let { this.put(FieldName.updatedAtFieldName, it) }
  return this
}

fun JsonObject.setUpdated(): JsonObject {
  return this.put(FieldName.updatedAtFieldName, Date())
}