package com.frankyung.moneytransfer.main

import com.frankyung.moneytransfer.service.MoneyTransferServiceImpl
import io.vertx.core.Future
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.logging.LoggerFactory
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.RoutingContext
import io.vertx.reactivex.ext.web.handler.BodyHandler
import io.vertx.core.DeploymentOptions
import io.vertx.core.json.JsonObject
import java.io.File
import java.io.InputStream

class MainVerticle : BaseAPIVerticle(){

    companion object {
        private const val DEFAULT_HOST = "0.0.0.0"
        private const val DEFAULT_PORT = 8787
        private const val UserCacheRebuildInterval = 15L // in minutes
        private val logger = LoggerFactory.getLogger(MainVerticle::class.java)
    }

    override fun start(future: Future<Void>) {
        super.start()
        logger.info("starting ...")

        val router = Router.router(vertx)
        val config = initConfig()
        router.route().handler(
                BodyHandler.create()
                        .setBodyLimit(20 * 1024 * 1024) // limit body to 10 MB
                        .setDeleteUploadedFilesOnEnd(true)
        )

        val apiConfig = config.getJsonObject("apiConfig").getJsonObject("gateway")
        val host = apiConfig.getString("host", DEFAULT_HOST)
        val port = apiConfig.getInteger("port", DEFAULT_PORT)

        initPackages()
        router.get("/api/health").handler(this::apiHealth)

        val verticles = listOf(
            MoneyTransferAPIVerticle()
        )

        verticles.forEach {
            verticle ->
            vertx.delegate.deployVerticle(verticle.setServicePackage(servicePackage), DeploymentOptions().setConfig(config())){
                router.mountSubRouter("/api${verticle.getApiPrefix()}", verticle.configRouter())
                logger.info("mounting /api${verticle.getApiPrefix()}")
            }
        }

        val httpServerOptions = HttpServerOptions()
        vertx.createHttpServer(httpServerOptions)
                .requestHandler(router::accept)
                .rxListen(port, host)
                .doOnSuccess { }
                .subscribe({
                    logger.info("API Gateway is running on port $port")
                    future.complete()
                }, { t ->
                    logger.error("API Gateway failed to run")
                    future.fail(t)
                })
    }
    private fun initPackages() {
        val moneyTransferService = MoneyTransferServiceImpl()
        servicePackage = ServicePackage(
                moneyTransferService = moneyTransferService)
    }

    private fun initConfig() :JsonObject{
        val inputStream: InputStream = File("config.json").inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }
        val config: JsonObject = JsonObject(inputString)
        return config
    }

    private fun apiHealth(context: RoutingContext) {
        context.response().end("ok")
    }

    override fun getApiPrefix(): String {
        TODO("No need to implement")
    }

    override fun configRouter(): Router {
        TODO("No need to implement")
    }
}