package com.frankyung.moneytransfer.main

import com.frankyung.moneytransfer.common.*
import com.frankyung.moneytransfer.models.Account
import io.vertx.core.json.JsonObject
import io.vertx.core.logging.LoggerFactory
import io.vertx.reactivex.ext.web.Router
import io.vertx.reactivex.ext.web.RoutingContext
import io.vertx.reactivex.ext.web.handler.BodyHandler
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*



class MoneyTransferAPIVerticle: BaseAPIVerticle() {

    companion object {
        internal val logger = LoggerFactory.getLogger(MoneyTransferAPIVerticle::class.java)!!
    }

    override fun getApiPrefix(): String = "/money-transfer"

    override fun configRouter(): Router {

        initAccounts()
        var router = Router.router(vertx)
        router.route().handler(BodyHandler.create())


        router.post("/account").responseHandler(this::createAccount)
        router.get("/accounts").responseHandler(this::getAllAccounts)
        router.get("/account/:id").responseHandler(this::getAccountById)
        router.get("/balance/:id").responseHandler(this::getBalance)
        router.post("/deposit/:id").responseHandler(this::deposit)
        router.post("/deposit/notify/:recId").responseHandler(this::depositNotify)
        router.post("/withdrawal/:id").responseHandler(this::withdrawal)
        router.post("/withdrawal/notify/:recId").responseHandler(this::withdrawalNotify)
        router.post("/transfer").responseHandler(this::transfer)
        router.get("/records/:id").responseHandler(this::transactionRecords)

        return router
    }

    private fun createAccount(context: RoutingContext) {
        var body = context.bodyAsJson
        body.shouldContains("accountName")
        var accountName = body.getValue("accountName").toString()

        if (accountName.isNullOrEmpty()) {
            throw MoneyTransferError(CommonErrorCode.NAME_MUST_NOT_BE_NULL_OR_EMPTY)
        }
        logger.info("Create account for " + accountName)

        var accountListSize = accounts.size.toString()
        val current = LocalDateTime.now()
        var formatter = DateTimeFormatter.ofPattern("yyMMddHHmmssSSS")
        val accountId = current.format(formatter) + accountListSize
        var account = Account(accountId, accountName,"GBP", BigDecimal(0), BigDecimal(0), Date(), Date())

        logger.info("Created new account: " + account)
        accounts.add(account)

        context.responseJson(JsonObject().put("result", "success").put("accountId", accountId))
    }

    private fun getAllAccounts(context: RoutingContext) {
        logger.info("Get account list")
        context.responseJson(JsonObject().put("result", "success").put("accounts", accounts))
    }

    private fun getAccountById(context: RoutingContext) {
        val accountId = context.request().getParam("id")
        logger.info("Get account of account number" + accountId)

        val account = servicePackage.moneyTransferService.getAccountById(accountId, accounts)

        if (account.id.isNullOrEmpty()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NOT_FOUND)
        }
        context.responseJson(JsonObject().put("result", "success").put("account", account.toJson()))
    }

    private fun getBalance(context: RoutingContext) {
        val accountId = context.request().getParam("id")
        logger.info("Get account balance of account " + accountId)

        val balanceMap = servicePackage.moneyTransferService.getAccountBalanceById(accountId, accounts)
        if (balanceMap.isEmpty()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NOT_FOUND)
        }

        val balance = JsonObject().put("balance", balanceMap.get("balance").toString()).put("availableBalance", balanceMap.get("availableBalance").toString())

        context.responseJson(JsonObject().put("result", balance))
    }

    private fun deposit(context: RoutingContext) {
        logger.info("Start to deposit...")
        var body = context.bodyAsJson
        body.shouldContains("accountName", "amount", "currency", "sign")

        //Assume the end user have perform the payment process

        var depositAmount = BigDecimal(0)
        try {
            depositAmount = body.getValue("amount").toString().toCustomBd()
        } catch (e : Exception){
            throw MoneyTransferError(CommonErrorCode.WRONG_AMOUNT_FORMAT)
        }
        if (depositAmount.compareTo(BigDecimal(0)) < 0){
            throw MoneyTransferError(CommonErrorCode.AMOUNT_MUST_BE_POSITIVE)
        }
        if (!verifySign(body.getValue("sign").toString())){
            throw MoneyTransferError(CommonErrorCode.WRONG_SIGN)
        }
        var curr = body.getValue("currency").toString()
        if (curr.toUpperCase() != "GBP"){
            throw MoneyTransferError(CommonErrorCode.SUPPORT_GBP_ONLY)
        }

        val accountNo = context.request().getParam("id")
        val account = servicePackage.moneyTransferService.getAccountById(accountNo, accounts)

        if (account.id.isNullOrEmpty()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NOT_FOUND)
        }

        var accountName = body.getValue("accountName").toString()
        if (account.accountName.toLowerCase() != accountName.toLowerCase()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NAME_ID_NOT_MATCH)
        }

        var recordId = servicePackage.moneyTransferService.createTransRecord(accountNo, depositAmount, curr, "Deposit",  "Pending", "", "", transactionRecords)

        //Normally the deposit api should return pending status and
        // wait for payment callback deposit notify api to finish the whole deposit process
        servicePackage.moneyTransferService.deposit(accountNo, depositAmount, accounts)
        servicePackage.moneyTransferService.updateRecordStatus(recordId, "success", transactionRecords)

        context.responseJson(JsonObject().put("result", "success").put("recordId", recordId))
    }

    private fun depositNotify(context: RoutingContext) {
        var body = context.bodyAsJson
        body.shouldContains("accountNo", "amount", "sign")

        val recordId = context.request().getParam("recId")
        val accountNo = body.getValue("accountNo").toString()
        val depositAmount = body.getValue("amount").toString().toCustomBd()

        if (!verifySign(body.getValue("sign").toString())){
            throw MoneyTransferError(CommonErrorCode.WRONG_SIGN)
        }

        servicePackage.moneyTransferService.deposit(accountNo, depositAmount, accounts)
        servicePackage.moneyTransferService.updateRecordStatus(recordId, "success", transactionRecords)

        context.responseJson(JsonObject().put("result", "success").put("recordId", recordId))
    }

    private fun withdrawal(context: RoutingContext) {
        logger.info("Start to withdrawal...")
        var body = context.bodyAsJson
        body.shouldContains("accountName", "amount", "currency", "sign")

        var withdrawalAmount = BigDecimal(0)
        try {
            withdrawalAmount = body.getValue("amount").toString().toCustomBd()
        } catch (e : Exception){
            throw MoneyTransferError(CommonErrorCode.WRONG_AMOUNT_FORMAT)
        }
        if (withdrawalAmount.compareTo(BigDecimal(0)) < 0){
            throw MoneyTransferError(CommonErrorCode.AMOUNT_MUST_BE_POSITIVE)
        }
        if (!verifySign(body.getValue("sign").toString())){
            throw MoneyTransferError(CommonErrorCode.WRONG_SIGN)
        }
        var curr = body.getValue("currency").toString()
        if (curr.toUpperCase() != "GBP"){
            throw MoneyTransferError(CommonErrorCode.SUPPORT_GBP_ONLY)
        }

        val accountNo = context.request().getParam("id")
        val account = servicePackage.moneyTransferService.getAccountById(accountNo, accounts)

        if (account.id.isNullOrEmpty()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NOT_FOUND)
        }

        var accountName = body.getValue("accountName").toString()
        if (account.accountName.toLowerCase() != accountName.toLowerCase()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NAME_ID_NOT_MATCH)
        }

        val availableBalance = account.availableBalance
        if (availableBalance.compareTo(withdrawalAmount) < 0){
            throw MoneyTransferError(CommonErrorCode.INSUFFICIENT_MONEY_IN_ACCOUNT)
        }

        var recordId = servicePackage.moneyTransferService.createTransRecord(accountNo, withdrawalAmount, curr, "Withdrawal",  "Pending", "","", transactionRecords)

        servicePackage.moneyTransferService.deductAvailableBalance(accountNo, withdrawalAmount, accounts)

        //As it is just a demo of the withdrawal process,
        // the process of dealing with the withdrawal money (e.g transfer to withdrawal money to other bank) is skipped

        //Normally the withdrawal api should return pending status and wait for payment callback withdrawal
        // notify api to finish the whole withdrawal process
        servicePackage.moneyTransferService.deductBalance(accountNo, withdrawalAmount, accounts)
        servicePackage.moneyTransferService.updateRecordStatus(recordId, "Success", transactionRecords)

        context.responseJson(JsonObject().put("result", "success").put("recordId", recordId))
    }

    private fun withdrawalNotify(context: RoutingContext) {
        var body = context.bodyAsJson
        body.shouldContains("accountNo", "amount", "sign")

        val recordId = context.request().getParam("recId")
        val accountNo = body.getValue("accountNo").toString()
        val withdrawalAmount = body.getValue("amount").toString().toCustomBd()

        if (!verifySign(body.getValue("sign").toString())){
            throw MoneyTransferError(CommonErrorCode.WRONG_SIGN)
        }

        servicePackage.moneyTransferService.deductBalance(accountNo, withdrawalAmount, accounts)
        servicePackage.moneyTransferService.updateRecordStatus(recordId, "Success", transactionRecords)

        context.responseJson(JsonObject().put("result", "success").put("recordId", recordId))
    }


    private fun transfer(context: RoutingContext) {
        logger.info("Start to transfer money...")
        //The whole transfer process involved
        // 1. withdrawal from one account and
        // 2. deposit to another account
        var body = context.bodyAsJson
        body.shouldContains("fromAccountNo", "amount", "currency", "toAccountNo", "toAccountName", "sign")

        if (!verifySign(body.getValue("sign").toString())){
            throw MoneyTransferError(CommonErrorCode.WRONG_SIGN)
        }
        var transferAmount = BigDecimal(0)
        try {
            transferAmount = body.getValue("amount").toString().toCustomBd()
        } catch (e : Exception){
            throw MoneyTransferError(CommonErrorCode.WRONG_AMOUNT_FORMAT)
        }
        if (transferAmount.compareTo(BigDecimal(0)) < 0){
            throw MoneyTransferError(CommonErrorCode.AMOUNT_MUST_BE_POSITIVE)
        }

        var curr = body.getValue("currency").toString()
        if (curr.toUpperCase() != "GBP"){
            throw MoneyTransferError(CommonErrorCode.SUPPORT_GBP_ONLY)
        }

        val fromAccountNo = body.getValue("fromAccountNo").toString()
        val fromAccount = servicePackage.moneyTransferService.getAccountById(fromAccountNo, accounts)
        if (fromAccount.id.isNullOrEmpty()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NOT_FOUND)
        }

        val toAccountNo = body.getValue("toAccountNo").toString()
        val toAccount = servicePackage.moneyTransferService.getAccountById(toAccountNo, accounts)
        if (toAccount.id.isNullOrEmpty()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NOT_FOUND)
        }

        var toAccountName = body.getValue("toAccountName").toString()
        if (toAccount.accountName.toLowerCase() != toAccountName.toLowerCase()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NAME_ID_NOT_MATCH)
        }

        val availableBalance = fromAccount.availableBalance
        if (availableBalance.compareTo(transferAmount) < 0){
            throw MoneyTransferError(CommonErrorCode.INSUFFICIENT_MONEY_IN_ACCOUNT)
        }

        var withdrawalRecId = servicePackage.moneyTransferService.createTransRecord(fromAccountNo, transferAmount, curr, "TrasferWithdrawal",  "Pending", "",toAccountNo, transactionRecords)
        var depositRecId = servicePackage.moneyTransferService.createTransRecord(toAccountNo, transferAmount, curr, "TransferDeposit",  "Pending", fromAccountNo,"", transactionRecords)

        servicePackage.moneyTransferService.deductAvailableBalance(fromAccountNo, transferAmount, accounts)

        servicePackage.moneyTransferService.deposit(toAccountNo, transferAmount, accounts)

        servicePackage.moneyTransferService.deductBalance(fromAccountNo, transferAmount, accounts)
        servicePackage.moneyTransferService.updateRecordStatus(withdrawalRecId, "Success", transactionRecords)

        servicePackage.moneyTransferService.updateRecordStatus(depositRecId, "success", transactionRecords)

        context.responseJson(JsonObject().put("result", "success"))
    }

    private fun transactionRecords(context: RoutingContext) {
        logger.info("Start to get transaction records...")
        val accountId = context.request().getParam("id")
        val account = servicePackage.moneyTransferService.getAccountById(accountId, accounts)

        if (account.id.isNullOrEmpty()){
            throw MoneyTransferError(CommonErrorCode.ACCOUNT_NOT_FOUND)
        }
        val records = servicePackage.moneyTransferService.getTransRecordByAccountId(accountId, transactionRecords)
        context.responseJson(JsonObject().put("result", "success").put("records", records))
    }

    private fun verifySign(sign: String): Boolean{
        //To make it simple and avoid any auth, always return true when verify sign
        return true
    }
}