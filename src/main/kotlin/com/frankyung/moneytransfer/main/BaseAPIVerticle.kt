package com.frankyung.moneytransfer.main

import com.frankyung.moneytransfer.models.Account
import com.frankyung.moneytransfer.models.TransactionRecord
import com.frankyung.moneytransfer.service.MoneyTransferService
import io.vertx.reactivex.core.AbstractVerticle
import io.vertx.reactivex.ext.web.Router
import java.math.BigDecimal
import java.util.*

data class ServicePackage(
        var moneyTransferService: MoneyTransferService
)


abstract class BaseAPIVerticle : AbstractVerticle() {
    lateinit var servicePackage: ServicePackage
    fun setServicePackage(servicePackage: ServicePackage): BaseAPIVerticle {
        this.servicePackage = servicePackage
        return this
    }

    abstract fun getApiPrefix(): String
    abstract fun configRouter(): Router

    var accounts = mutableListOf<Account>()
    var transactionRecords = mutableListOf<TransactionRecord>()

    fun initAccounts(){
        accounts.add(Account("1", "Frank","GBP", BigDecimal(1000), BigDecimal(1000), Date(), Date()))
        accounts.add(Account("2", "Mary","GBP", BigDecimal(1000), BigDecimal(1000), Date(), Date()))
    }

}