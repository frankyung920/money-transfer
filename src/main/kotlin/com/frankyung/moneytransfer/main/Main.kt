package com.frankyung.moneytransfer.main

import io.vertx.core.Vertx

fun main(args: Array<String>) {
    Vertx.vertx().deployVerticle(MainVerticle::class.java.name)
}
