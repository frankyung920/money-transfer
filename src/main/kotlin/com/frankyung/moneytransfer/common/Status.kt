package com.frankyung.moneytransfer.common

enum class Status(val value: String) {

    PENDING("Pending"),
    REJECTED("Rejected"),
    APPROVED("Approved");

    companion object {
        private val map = Status.values().associateBy(Status::value);
        fun fromString(type: String): Status = when {
            map.containsKey(type) -> map[type]!!
            map.containsValue(valueOf(type)) -> valueOf(type)
            else -> throw IllegalArgumentException("$type is not supported by EnumClass ${Status::class.java.name}")
        }
    }
}