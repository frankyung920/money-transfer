package com.frankyung.moneytransfer.common

import io.vertx.core.json.JsonObject

enum class CommonErrorCode(override val value: String) : IErrorCode {
    UNKNOWN("unknown"),
    INVALID_REQUEST("invalid-request"),
    ACCOUNT_NOT_FOUND("account-not-found"),
    ACCOUNT_NAME_ID_NOT_MATCH("account-name-and-id-not-match"),
    NAME_MUST_NOT_BE_NULL_OR_EMPTY("name-must-not-be-null-or-empty"),
    WRONG_AMOUNT_FORMAT("wrong-amount-format"),
    AMOUNT_MUST_BE_POSITIVE("amount-must-be-positive"),
    WRONG_SIGN("wrong-sign"),
    SUPPORT_GBP_ONLY("support-gbp-only"),
    INSUFFICIENT_MONEY_IN_ACCOUNT("insufficient-money-in-account")
}

class MoneyTransferError : Exception {
    val status: Int
    val code: IErrorCode
    val note: String?
    val data: JsonObject?

    constructor(){
        this.status = 500
        this.code = CommonErrorCode.UNKNOWN
        this.note = CommonErrorCode.UNKNOWN.value
        this.data = null
    }

    constructor(code: IErrorCode, note: String? = null, data: JsonObject? = null) {
        this.status = 400
        this.code = code
        this.note = note
        this.data = data
    }

    constructor(status: Int = 500, code: IErrorCode, note: String? = null, data: JsonObject? = null) {
        this.status = status
        this.code = code
        this.note = note
        this.data = data
    }

    override val message: String?
        get() = this.toJson().encode()

    constructor(jsonObject: JsonObject) : this(
            status = jsonObject.getInteger(statusFieldName, 500),
            code = jsonObject.getString(codeFieldName).let {
                if (it == null) {
                    CommonErrorCode.UNKNOWN
                } else {
                    object : IErrorCode {
                        override val value: String
                            get() = it
                    }
                }
            },
            note = jsonObject.getString(noteFieldName),
            data = jsonObject.getJsonObject(dataFieldName)
    )

    override fun getLocalizedMessage(): String {
        return code.value
    }

    override fun toString(): String {
        return toJson().encode()
    }

    fun toJson(): JsonObject {
        val json = JsonObject()
        json.put(statusFieldName, status)
        json.put(codeFieldName, code.value)
        json.put(noteFieldName, note)
        json.put(dataFieldName, data)
        return json
    }

    companion object {
        val statusFieldName = "status"
        val codeFieldName = "code"
        val noteFieldName = "note"
        val dataFieldName = "data"

        inline fun fromError(error: Exception): MoneyTransferError {
            return try {
                val json = JsonObject(error.message)
                MoneyTransferError(json)
            } catch (e: Exception) {
                MoneyTransferError(
                    status = 500,
                    code = CommonErrorCode.UNKNOWN,
                    note = error.message
                )
            }
        }

        inline fun fromError(error: Throwable): MoneyTransferError {
            return try {
                val json = JsonObject(error.message)
                MoneyTransferError(json)
            } catch (e: Exception) {
                MoneyTransferError(
                    status = 500,
                    code = CommonErrorCode.UNKNOWN,
                    note = error.message
                )
            }
        }
    }
}
