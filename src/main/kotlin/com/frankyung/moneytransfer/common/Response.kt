package com.frankyung.moneytransfer.common

import io.reactivex.exceptions.CompositeException
import io.vertx.core.json.JsonObject
import io.vertx.reactivex.ext.web.Route
import io.vertx.reactivex.ext.web.RoutingContext
import io.vertx.serviceproxy.ServiceException

fun RoutingContext.responseJson(json: JsonObject, statusCode: Int = 200) {
    this.response()
            .setStatusCode(statusCode)
            .putHeader("content-type", "application/json")
            .putHeader("Cache-Control", "no-store, no-cache")
            .end(json.encode())
}

fun RoutingContext.responseSuccess() {
    this.response()
            .setStatusCode(200)
            .putHeader("content-type", "application/json")
            .putHeader("Cache-Control", "no-store, no-cache")
            .end(JsonObject().put("success", true).encode())
}

fun RoutingContext.responseError(statusCode: Int, errorCode: IErrorCode, message: String? = null, data: JsonObject? = null) {
    if (this.response().ended()) return
    val responseJson = JsonObject()
    responseJson.put("statusCode", statusCode)
    responseJson.put("errorCode", errorCode.value)
    message?.let { responseJson.put("message", it) }
    data?.let { responseJson.put("data", it) }

    this.response()
            .setStatusCode(statusCode)
            .putHeader("content-type", "application/json")
            .putHeader("Cache-Control", "no-store, no-cache")
            .end(responseJson.encode())
}


fun RoutingContext.responseError(error: MoneyTransferError) {
    this.responseError(error.status, error.code, error.note, error.data)
}

inline fun Route.responseHandler(crossinline handler: (RoutingContext) -> Unit) {
    this.handler { rc ->
        try {
            handler(rc)
        } catch (e: MoneyTransferError) {
            e.printStackTrace()
            rc.responseError(MoneyTransferError.fromError(e))
        } catch (e: Exception) {
            e.printStackTrace()
            rc.responseError(MoneyTransferError.fromError(e))
        }
    }
}
