package com.frankyung.moneytransfer.common

import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import java.math.BigDecimal
import io.vertx.kotlin.core.json.get
import java.math.RoundingMode


fun String.toCustomBd(): BigDecimal {
    return this.toBigDecimal().setScale(2, RoundingMode.HALF_DOWN)
}


interface IErrorCode {
    val value: String
    fun toError(statusCode: Int = 400, note: String? = null, data: JsonObject? = null): MoneyTransferError = MoneyTransferError(statusCode, this, note, data)
}

fun JsonObject.shouldContains(vararg keys: String, errorCode: IErrorCode = CommonErrorCode.INVALID_REQUEST) {
    val missingKeys = keys.filter { !this.containsKey(it) }
    if (missingKeys.isNotEmpty()) {
        val missingKeysArray = JsonArray()
        missingKeys.forEach { missingKeysArray.add(it) }
        val data = JsonObject().put("missingKeys", missingKeys)
        throw errorCode.toError(400, "Request body should contains key(s) <${missingKeys.joinToString(">, <")}>", data)
    }
}
