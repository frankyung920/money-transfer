# Money Transfer API
The Money Transfer API is using Vert.x Kotlin to develop, please browse https://vertx.io/docs/vertx-core/kotlin/ for more information about Vert.x.

The Money Transfer API contains several api for performing 
1. Money Transfer between accounts

2. Create account

3. Get all accounts

4. Get account by account number

5. Get account balance and available balance

6. Deposit (There is also deposit notify api for updating record as success after payment callback, but to simplify, now the deposit api will mark status as success)

7. Withdrawal (There is also withdrawal notify api for updating record as after checking available balance and withdrwal money to customer other bank(or other way), , but to simplify, now the withdrawal api will mark status as success)

8. Get transaction records by account number

There is a sign field in request body for Deposit, Withdrawal and Money Transfer, which is a MD5 hash string of request param + api key, but to avoid any authentication, the verify process always return true so that the authentication can be passed.

To keep the data source run in memory, 2 variables **accounts** (for storing account information) and **transactionRecords** (for storing transaction records).

---

### To build and run the api
The application uses gradle build, a standalone jar could be exported by using gradle. 

In build.gradle, by running "jar", the jar could be exported (located in build/libs). 
Please noted that the config.json must be placed with the jar file (in the same folder/path).

```
jar {
    from('src/main/kotlin') {
        include '../../../*.json'
    }
    manifest {
        attributes 'Main-Class': 'com.frankyung.moneytransfer.main.MainKt'
    }
    from { configurations.compile.collect { it.isDirectory() ? it : zipTree(it) } }
}
```

A pre built jar is also contain in the project folder (money-transfer-1.0-SNAPSHOT.jar).

```
Use below command to run the application.

java -jar money-transfer-1.0-SNAPSHOT.jar 
```
The main class is located in src/kotlin/com/frankyung/moneytransfer/main/Main.kt, by using any IDE, the application also could be run (by running the Main.kt).

### About the application
The api only can handle only one currency (GBP) and using half down with 2 decimal places.

2 accounts will be created with 1000 GBP balance by default for convenience, below are the 2 information of the accounts.

1.  Account Name: Frank, Account Number: 1

2.  Account Name: Mary, Account Number: 2

```
"accounts": [
        {
            "id": "1",
            "accountName": "Frank",
            "currency": "GBP",
            "balance": 1000,
            "availableBalance": 1000,
            "createdAt": 1566143246582,
            "updatedAt": 1566143246582
        },
        {
            "id": "2",
            "accountName": "Mary",
            "currency": "GBP",
            "balance": 1000,
            "availableBalance": 1000,
            "createdAt": 1566143246582,
            "updatedAt": 1566143246582
        }
    ]
```
---
### Test / Demonstration
The project contain a postman collection json file (money transfer.postman_collection.json).  

By importing in Postman and run in Postman collection runner. All the api could be tested.

There is also a health check api to check whether the application is up or down.
```
Request URL : localhost:8787/api/health
Method: GET

Response:
ok
```

---
### API URL and the example request & response

##### 1. Money Transfer between accounts
```
Request URL: localhost:8787/api/money-transfer/transfer
Method: POST
```
```
Sample Request:
{
	"fromAccountNo": "1", 
	"amount": 100.01, 
	"currency": "GBP",
	"toAccountNo": "2",
	"toAccountName": "Mary" ,
	"sign": ""
}

Sample Response:
{
    "result": "success"
}

```
##### 2. Create accounts
```
Request URL: localhost:8787/api/money-transfer/account
Method: POST
```
```
Sample Request:
{
	"accountName": "Amy" 
}

Sample Response:
{
    "result": "success",
    "accountId": "1908182016194152"
}

The account number is constructed by current timestamp + an ascending number (except the 2 default created account) so that it will be unique.
```

##### 3. Get all accounts
```
Request URL: localhost:8787/api/money-transfer/accounts
Method: GET
```
```
Sample Response:
{
    "result": "success",
    "accounts": [
        {
            "id": "1",
            "accountName": "Frank",
            "currency": "GBP",
            "balance": 1000,
            "availableBalance": 1000,
            "createdAt": 1566155773695,
            "updatedAt": 1566155773695
        },
        {
            "id": "2",
            "accountName": "Mary",
            "currency": "GBP",
            "balance": 1000,
            "availableBalance": 1000,
            "createdAt": 1566155773695,
            "updatedAt": 1566155773695
        },
        {
            "id": "1908182016194152",
            "accountName": "Amy",
            "currency": "GBP",
            "balance": 0,
            "availableBalance": 0,
            "createdAt": 1566155779441,
            "updatedAt": 1566155779441
        }
    ]
}
```
##### 4. Get account by account number
```
Request URL: localhost:8787/api/money-transfer/account/:id
where :id is the account number
Method: GET
```
```
Sample Response:
{
    "result": "success",
    "account": {
        "id": "1",
        "accountName": "Frank",
        "currency": "GBP",
        "balance": "1000",
        "availableBalance": "1000",
        "createdAt": "Sun Aug 18 20:16:13 GMT 2019",
        "updatedAt": "Sun Aug 18 20:16:13 GMT 2019"
    }
}
```
##### 5. Get account balance and available balance
```
Request URL: localhost:8787/api/money-transfer/balance/:id
where :id is the account number
Method: GET
```
```
Sample Response:
{
    "result": {
        "balance": "1000",
        "availableBalance": "1000"
    }
}
```
##### 6. Deposit 
```
Request URL: localhost:8787/api/money-transfer/deposit/:id
where :id is the account number
Method: POST
```
```
Sample Request:
{
	"accountName" : "Frank",
	"amount":500,
	"currency": "GBP",
	"sign": ""
}

Sample Response:
{
    "result": "success",
    "recordId": "1"
}
```
##### 7. Withdrawal
```
Request URL: localhost:8787/api/money-transfer/withdrawal/:id
where :id is the account number
Method: POST
```
```
Sample Request:
{
	"accountName" : "Frank",
	"amount":100,
	"currency": "GBP",
	"sign": ""
}
Sample Response:
{
    "result": "success",
    "recordId": "2"
}
```

##### 8. Get transaction records by account number
```
Request URL: localhost:8787/api/money-transfer/records/:id
where :id is the account number
Method: GET
```
```
Sample Response:
{
    "result": "success",
    "records": [
        {
            "id": "1",
            "accountNo": "1",
            "currency": "GBP",
            "amount": "500.00",
            "type": "Deposit",
            "status": "success",
            "fromAccountNo": "",
            "targetAccountNo": "",
            "createdAt": "Sun Aug 18 20:22:29 GMT 2019",
            "updatedAt": "Sun Aug 18 20:22:29 GMT 2019"
        },
        {
            "id": "2",
            "accountNo": "1",
            "currency": "GBP",
            "amount": "100.00",
            "type": "Withdrawal",
            "status": "Success",
            "fromAccountNo": "",
            "targetAccountNo": "",
            "createdAt": "Sun Aug 18 20:23:59 GMT 2019",
            "updatedAt": "Sun Aug 18 20:23:59 GMT 2019"
        },
        {
            "id": "3",
            "accountNo": "1",
            "currency": "GBP",
            "amount": "100.01",
            "type": "TrasferWithdrawal",
            "status": "Success",
            "fromAccountNo": "",
            "targetAccountNo": "2",
            "createdAt": "Sun Aug 18 20:24:41 GMT 2019",
            "updatedAt": "Sun Aug 18 20:24:41 GMT 2019"
        }
    ]
}
```

